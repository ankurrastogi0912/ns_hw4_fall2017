from scapy.all import *
from datetime import datetime
import sys

interface = None
tracefile = None
expression = None
passLoop = 0
tracefileData = None
tracefileFlag = 0

for index, arg in enumerate(sys.argv):
	if index == 0:
		continue
	if(passLoop == 1):
		passLoop = 0
		continue
	if(arg == "-i" and index < (len(sys.argv) - 1)):
		interface = str(sys.argv[index + 1])
		passLoop = 1
	elif(arg == "-r" and index < (len(sys.argv) - 1)):
                tracefile = str(sys.argv[index + 1])
		tracefileData = rdpcap(tracefile)
   		tracefileFlag = 1
	        passLoop = 1
	else:
		expression = str(arg)

#setting default
if(expression == None):
	expression = str("udp port 53")

if(interface == None):
	interface = str("ens33")

dns_packet_list = []

def checkPacketForInjection(dns_resp_dict, dns_packet_list, sPacket, i):
	for pkt in dns_packet_list:
		if pkt['id'] == dns_resp_dict['id'] and pkt['dstIp'] == dns_resp_dict['dstIp']:
			if dns_resp_dict['resolvedIp'] not in pkt['resolvedIp']:
				if dns_resp_dict['ttl'] != pkt['ttl']:
					print ""
					print str(datetime.now()), "DNS Poisioning Attempt"
					print "TXID ", hex(pkt['id']), "Request ", dns_resp_dict['hostname']
					print "Answer 1 ", dns_resp_dict['resolvedIp']
					print "Answer 2 ", pkt['resolvedIp']
					print ""
	

def trafficSniffer(sPacket):
	if IP in sPacket:
		victim_ip = sPacket[IP].src
		src_dns_ip = sPacket[IP].dst
		if sPacket.haslayer(DNS) and sPacket.getlayer(DNS).qr == 1:		
			if sPacket.haslayer(DNSRR) and sPacket[DNS].qd.qtype == 1:
				a_count = sPacket[DNS].ancount
				i = a_count + 4
				dns_resp_dict = {}
				while i > 4:
					if(sPacket[0][i].type == 1):
						if(not dns_resp_dict.has_key('id')):
							dns_resp_dict['id'] = sPacket[DNS].id
							dns_resp_dict['ttl'] = sPacket[0][i].ttl
							dns_resp_dict['dstIp'] = sPacket[IP].dst
							dns_resp_dict['srcIp'] = sPacket[IP].src
							dns_resp_dict['hostname'] = sPacket[0][i].rrname.rstrip(".")
							dns_resp_dict['resolvedIp'] = [sPacket[0][i].rdata]
						else :
							dns_resp_dict['resolvedIp'].append(sPacket[0][i].rdata)
						
					i -= 1
				if dns_resp_dict.has_key('id'):
					checkPacketForInjection(dns_resp_dict, dns_packet_list, sPacket, i);
					dns_packet_list.append(dns_resp_dict);

				
if(tracefileFlag == 1):
	sniff(prn=trafficSniffer,offline=tracefileData,filter=expression)
else:
	sniff(prn=trafficSniffer,iface=interface,store=0,filter=expression)
